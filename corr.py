import math
#import matplotlib.pyplot as plt\

x = [i/10. for i in range(200)]
shift1 = float(input("Enter shift: ")) # is unknown to you need to find
y1 = [1.3 * math.sin(i + shift1) for i in x] # cannot manipulate y1

def find_shift():
    s_max=-1
    shift2 = 0 
    for value in [i/10. for i in range(90)]:
        y2 = [1.3*math.sin(i + shift1 - value) for i in x]
        corr = [y1[i]*y2[i] for i in range(len(x))]
        s = sum(corr)/len(x) * 2 # 2 for normalizing
        #plt.plot(x,corr)
        if (s>s_max):
            s_max = s
            shift2 = value
    return shift2

print("Shift needed is " + str(find_shift()))
#plt.show()