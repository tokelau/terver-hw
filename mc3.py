import random as rnd

x = [i/2 for i in range(8)]
y = lambda x: x**0.5
square = 3.5*y(x[7])
N=0
N_all=10000
the_area = 0
for i in x:
	the_area+= y(i)*0.5
print ("The exact value: {0:.5}".format(the_area))
for j in range(len(x)):
	for i in range(N_all):
		if(rnd.uniform(0, y(x[7]))<=y(x[j])):
			N+=1
N = N*square/N_all/8
print("The experimental value: {0:.5}".format(N))